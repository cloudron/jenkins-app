// https://github.com/foxylion/docker-jenkins
// https://github.com/jenkinsci/docker/issues/310

import jenkins.model.*
import hudson.security.*

def jenkins = Jenkins.getInstance()

if (jenkins.getSecurityRealm() instanceof LDAPSecurityRealm) {
    println "==> Will not auto-create user for LDAP"
    return 0
}

println "==> Creating local user 'admin'"

// https://javadoc.jenkins.io/hudson/security/HudsonPrivateSecurityRealm.html
if (!(jenkins.getSecurityRealm() instanceof HudsonPrivateSecurityRealm))
    jenkins.setSecurityRealm(new HudsonPrivateSecurityRealm(false))

//if (!(jenkins.getAuthorizationStrategy() instanceof GlobalMatrixAuthorizationStrategy))
//    jenkins.setAuthorizationStrategy(new GlobalMatrixAuthorizationStrategy())

def user = jenkins.getSecurityRealm().createAccount('admin', 'changeme')
user.save()
jenkins.getAuthorizationStrategy().add(Jenkins.ADMINISTER, 'admin')

jenkins.save()
