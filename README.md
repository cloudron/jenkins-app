## Various links

* LDAP Plugin - https://github.com/jenkinsci/ldap-plugin
* What to backup - https://jenkins.io/doc/book/architecting-for-scale/
* Groovy scripts - https://cheatsheet.dennyzhang.com/cheatsheet-jenkins-groovy-a4
    https://github.com/samrocketman/jenkins-script-console-scripts
* systemd - https://github.com/open-mpi/ompi/wiki/Jenkins-Build-Agent

