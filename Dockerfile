FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV JENKINS_VERSION 2.199

ENV REF /app/code/ref
RUN mkdir -p /app/code/ref/init.groovy.d
WORKDIR /app/code

# jdk has jar and jre has java executables
RUN apt-get update && \
    apt-get -y install --no-install-recommends openjdk-11-jdk && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# this is here so that exec containers get them
ENV JENKINS_HOME /app/data/jenkins_home
RUN curl -fsSL https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/${JENKINS_VERSION}/jenkins-war-${JENKINS_VERSION}.war -o /app/code/jenkins.war

# these helper scripts allow us to download plugins into the Docker image into REF
RUN curl -L https://raw.githubusercontent.com/jenkinsci/docker/master/jenkins-support -o /usr/local/bin/jenkins-support && \
    curl -L https://raw.githubusercontent.com/jenkinsci/docker/master/install-plugins.sh  -o /usr/local/bin/install-plugins.sh && \
    chmod +x /usr/local/bin/install-plugins.sh /usr/local/bin/jenkins-support

RUN JENKINS_WAR=/app/code/jenkins.war JENKINS_UC=https://updates.jenkins.io /usr/local/bin/install-plugins.sh \
    authorize-project \
    blueocean \
    credentials-binding \
    docker-slaves \
    docker-workflow \
    git \
    ldap \
    matrix-auth \
    workflow-aggregator

COPY start.sh /app/code/
COPY config_templates /app/code/config_templates
COPY default-user.groovy $REF/init.groovy.d/

CMD [ "/app/code/start.sh" ]
