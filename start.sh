#!/bin/bash

set -eu

export JENKINS_HOME=/app/data/jenkins_home
export TPL_DIR=/app/code/config_templates
mkdir -p /app/data/jenkins_home/secrets /run/jenkins

if [[ ! -f ${JENKINS_HOME}/config.xml ]]; then
    echo "==> Creating config.xml on first run"
    if [[ -n "${CLOUDRON_LDAP_URL:-}" ]]; then
        cp ${TPL_DIR}/config-sso.xml ${JENKINS_HOME}/config.xml
    else
        cp ${TPL_DIR}/config-no-sso.xml ${JENKINS_HOME}/config.xml
    fi
fi

# Agent → Master Security
echo "false" > ${JENKINS_HOME}/secrets/slave-to-master-security-kill-switch

echo "==> Creating location config"
xmlstarlet ed \
    --update "//jenkins.model.JenkinsLocationConfiguration/jenkinsUrl" -v "${CLOUDRON_APP_ORIGIN}" \
    ${TPL_DIR}/jenkins.model.JenkinsLocationConfiguration.xml > ${JENKINS_HOME}/jenkins.model.JenkinsLocationConfiguration.xml
 
echo "==> Creating queue item configuration"
[[ ! -f ${JENKINS_HOME}/jenkins.security.QueueItemAuthenticatorConfiguration.xml ]] && cp ${TPL_DIR}/jenkins.security.QueueItemAuthenticatorConfiguration.xml ${JENKINS_HOME}/jenkins.security.QueueItemAuthenticatorConfiguration.xml

echo "==> Creating mailer config"
xmlstarlet ed \
    --update "//hudson.tasks.Mailer_-DescriptorImpl/authentication/username" -v "${CLOUDRON_MAIL_SMTP_USERNAME}" \
    --update "//hudson.tasks.Mailer_-DescriptorImpl/authentication/password" -v "${CLOUDRON_MAIL_SMTP_PASSWORD}" \
    --update "//hudson.tasks.Mailer_-DescriptorImpl/replyToAddress" -v "${CLOUDRON_MAIL_FROM}" \
    --update "//hudson.tasks.Mailer_-DescriptorImpl/smtpHost" -v "${CLOUDRON_MAIL_SMTP_SERVER}" \
    --update "//hudson.tasks.Mailer_-DescriptorImpl/smtpPort" -v "${CLOUDRON_MAIL_SMTP_PORT}" \
    ${TPL_DIR}/hudson.tasks.Mailer.xml > ${JENKINS_HOME}/hudson.tasks.Mailer.xml

echo "==> Installing plugins (from ${REF})"
# see https://github.com/jenkinsci/docker/blob/master/jenkins.sh
touch /run/jenkins/copy_reference.log
COPY_REFERENCE_FILE_LOG=/run/jenkins/copy_reference.log find "${REF}" \( -type f -o -type l \) -exec bash -c '. /usr/local/bin/jenkins-support; for arg; do copy_reference_file "$arg"; done' _ {} +

echo "==> Updating config.xml"
# the manager password will get re-encrypted by jenkins on a restart (https://stackoverflow.com/questions/25364611/jenkins-how-to-change-ldap-password)
# currently, we can only use LDAP or local auth separately
# https://issues.jenkins-ci.org/browse/JENKINS-3404 https://stackoverflow.com/questions/54275615/how-to-login-to-jenkins-when-ldap-server-is-not-available
# xmlstarlet does not support 1.1. It writes in double quotes, jenkins writes in single quotes :/
sed -e "s/version=.1.1./version='1.0'/" -i ${JENKINS_HOME}/config.xml
xmlstarlet ed --inplace \
    --update "//hudson/version" -v "${JENKINS_VERSION}" \
    --update "//hudson/slaveAgentPort" -v "${SLAVE_PORT:-0}" \
    ${JENKINS_HOME}/config.xml

if [[ -n "${CLOUDRON_LDAP_URL:-}" ]]; then
    echo "==> Updating ldap configuration"
    xmlstarlte ed --inplace \
        --update "//hudson/securityRealm/configurations/jenkins.security.plugins.ldap.LDAPConfiguration/server" -v "${CLOUDRON_LDAP_URL}" \
        --update "//hudson/securityRealm/configurations/jenkins.security.plugins.ldap.LDAPConfiguration/rootDN" -v "${CLOUDRON_LDAP_USERS_BASE_DN}" \
        --update "//hudson/securityRealm/configurations/jenkins.security.plugins.ldap.LDAPConfiguration/managerDN" -v "${CLOUDRON_LDAP_BIND_DN}" \
        --update "//hudson/securityRealm/configurations/jenkins.security.plugins.ldap.LDAPConfiguration/managerPasswordSecret" -v "${CLOUDRON_LDAP_BIND_PASSWORD}" \
        ${JENKINS_HOME}/config.xml
fi

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/jenkins

# https://groups.google.com/forum/#!topic/jenkinsci-users/Pb4QZVc2-f0
# https://wiki.jenkins-ci.org/display/JENKINS/Features+controlled+by+system+properties
echo "==> Starting jenkins"
# session timeout (forceful logout) is minutes, session eviction (inactivity) seconds
exec /usr/local/bin/gosu cloudron:cloudron java -Dhudson.model.UpdateCenter.never=true -Djenkins.install.runSetupWizard=false -Duser.home="$JENKINS_HOME" -DsessionTimeout=10080 -DsessionEviction=518400 -jar /app/code/jenkins.war

