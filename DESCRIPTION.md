This app packages Jenkins <upstream>11.0.3</upstream>

Jenkins is the leading open source automation server, Jenkins provides hundreds
of plugins to support building, deploying and automating any project. 

## Features

* Continuous Integration and Continuous Delivery
* Easy configuration
* Plugins
* Extensible
* Distributed

