Jenkins is pre-configured to authenticate with Cloudron user management.
By default, all users are admins. You can change this based on your
requirements by following the docs.

By default, building on master is disabled.
